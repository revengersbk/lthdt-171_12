public class Warrior extends Fighter {


    public Warrior(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        double combatScore;
        if (super.getWp() == (double) 1) combatScore = (double) super.getBaseHp();
        else combatScore = (double) super.getBaseHp()/ (double) 10;
        if (Utility.isPrime(Battle.GROUND)) combatScore = (double) 2*super.getBaseHp();
        return combatScore;
    }
}
