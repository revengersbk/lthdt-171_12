import com.sun.org.apache.xpath.internal.SourceTree;

public class Paladin extends Knight {
    //final static double golden = ((double) 1 + Math.sqrt(5)) / (double) 2;

    public Paladin(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore(){
        double combatScore = (double) 3*super.getBaseHp();
        if (Utility.isFibo(super.getBaseHp())){
            combatScore = 1000 + Utility.findFiboIndex(super.getBaseHp());
        }
        //if (Utility.isFibo(super.getBaseHp()))
            //combatScore = 1000 + Math.floor((Math.log(super.getBaseHp()* Math.sqrt(5) + (1/2))) / Math.log(golden));
        return combatScore;
    }

    /*public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            if (Utility.isFibo(i)) {
                double combatScore = Utility.findFiboIndex(i);
                System.out.println(i + " : " + combatScore);
            }
        }
    }*/
}
