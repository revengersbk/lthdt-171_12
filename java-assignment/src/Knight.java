public class Knight extends Fighter {


    public Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        double combatScore;
        if (super.getWp() == (double) 1) combatScore = (double) super.getBaseHp();
        else combatScore = (double) super.getBaseHp()/ (double) 10;
        if (Utility.isSquare(Battle.GROUND)) combatScore = (double) 2*super.getBaseHp();
        return combatScore;
    }
}
