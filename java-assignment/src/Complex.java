/**
 * This class represents a complex number <code>z = x + y*i</code>
 */
public class Complex {
	private double mRe;
	private double mIm;
	public Complex(double re, double im){
		mRe = re;
		mIm = im;
	}
	/**
	 * Calculate the magnitude of the complex number. For number <code>z = x + y*i</code>, 
	 * <code>|z| = sqrt(x*x + y*y)</code>.
	 * @return the magnitude value.
	 */
	public double getMagnitude() {
		return Math.sqrt(mRe * mRe + mIm * mIm);
	}
	public double getRe() {
		return mRe;
	}
	public double getIm() {
		return mIm;
	}
	public void setRe(double x) {
		mRe = x;
	}
	public void setIm(double y) {
		mIm = y;
	}
	
	@Override
	public String toString() {
		return String.format("C{%f,%f}", mRe, mIm);
	}
}
