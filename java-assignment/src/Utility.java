public class Utility {

    /**
     * Test whether a specific num is a prime num.
     *
     * @param num the num
     * @return <code>true</code> if <code>num</code> is a prime num.
     */
    public static boolean isPrime(int num) {
        if (num % 2 == (double) 0) {
            return false;
        }
        for (int i = 3; i*i < num; i += 2) {
            if (num % i == (double) 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Test whether a specific number is a square number.
     *
     * @param num the number
     * @return <code>true</code> if <code>num</code> is a square number.
     */
    public static boolean isSquare(int num) {
        int x = (int) Math.sqrt(num);
        return x * x == num;
    }

    public static boolean isFibo(int num) {
        return isSquare(5*num*num + 4) || isSquare(5*num*num - 4);
    }

    public static int findFiboIndex(int n)
    {
        if (!isFibo(n)) return 0;
        if (n <= 1)
            return n + 1;

        /*if (n == 1)
            return 2;*/

        int a = 0, b = 1, c = 1;
        int res = 1;
        while (c < n)
        {
            res++;
            c = a + b;
            a = b;
            b = c;
        }
        return res;
    }

    /*public static int fib(int num) {
        if (num <= 1) return num;
        return fib(num - 1) + fib(num - 2);
    }



    public static int getNFibo(int num) {
        int count = 0;
        int i = 0;
        int temp;
        while (i < num){
            temp = fib(i);
            if(temp != num && temp < num) count++;
            i++;
        }
        return count;
    }*/
}
